const pool = require('db');

var getUser = function(alexa_uid,callback) {
	pool.connect().then(function(client) {
			
	      client.query('select confirmed, email, uid, alexa_uid, confirmation_code,bucket from users where alexa_uid=$1', [alexa_uid], function(err, result) {
	    	if(err){	
	    		console.log(err);
	    		return err;
	    	}
	        // you MUST return your client back to the pool when you're done!
	        client.release();
	        console.log("User " + JSON.stringify(result.rows[0]));
	        callback(err, result.rows[0]);
	      });
	    });

}

var linkAccount = function(code, alexa_uid, callback) {
	pool.connect().then(function(client) {
		
	      client.query('select confirmed, email, uid, alexa_uid, confirmation_code from users where confirmed=false and confirmation_code=$1', [code], function(err, result) {
	    	if(err){
	    		client.release();	
	    		console.log(err);
	    		return err;
	    	}
	        // you MUST return your client back to the pool when you're done!
	        client.release();
	        var user = result.rows[0];
	        console.log("User " + JSON.stringify(user))
	        console.log("alexa_uid " + alexa_uid);
	        if(user) {
	        	 var query = "UPDATE users SET alexa_uid = ($1), confirmed=($2) WHERE uid=($3)";
	        	 client.query(query, [alexa_uid,true,user.uid], function(err, result) {
	        		 if(err){
	     	    		console.log(err);
	     	    		return err;
	     	    	 }
	        		 callback(err, result.rows[0]);
	        	 })
	        } else {
	        	callback(err, "NO_USER_FOUND");
	        }
	        
	        
	        
	      });
	    });
}
module.exports.linkAccount = linkAccount;
module.exports.getUser = getUser;

