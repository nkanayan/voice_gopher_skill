//var delim = getDelim("asasadsa.asasas.dasdasdas\demo.tsv");
//console.log(delim)

function getExtension(filename) {
    var ext = filename.split('.');
    return ext[ext.length - 1];
}

function getDelimByFilename(filename) {
	var extension = getExtension(filename) ;
	return getDelim(extension);
}

function getDelim(contentType) {
	console.log("contentType : " + contentType);
	if(contentType=='csv') {
		return ',';
	} else if (contentType =='tsv') {
		return '\t';
	} else if (contentType =='plain') {
		console.log("plain ; ");
		return ',';
	} else {
		return 'unkown format';
	}
} 

function getDelimByContentType(contentType) {
	console.log("contentType : " + contentType);
	var type = contentType.split('/');
    return getDelim(type[type.length - 1]);
	
}

module.exports.getDelim = getDelim;
module.exports.getDelimByContentType = getDelimByContentType;