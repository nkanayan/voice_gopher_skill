	
	
	'use strict';

	var AlexaSkill = require('./AlexaSkill');
	var APP_ID = undefined; //replace with 'amzn1.echo-sdk-ams.app.[your-unique-value-here]';
	var AWS = require('aws-sdk');
	var security = require('security');
	var delimDetector = require('utils');
	var accountNotLinkedtext = "Please link to your Amazon account by saying Alexa tell VoiceGopher to link my account with code ";
	var minCardHeading = "Lowest Value results";
	var maxCardHeading = "Highest Value results";
	var avgCardHeading = "Average Value results";
	var stDevCardHeading = "Standard Deviation Value results";
	var sumCardHeading = "Total Value results";
	
	const columnNames= ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ','CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ','DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ','EA','EB','EC','ED','EE','EF','EG','EH','EI','EJ','EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ','FA','FB','FC','FD','FE','FF','FG','FH','FI','FJ','FK','FL','FM','FN','FO','FP','FQ','FR','FS','FT','FU','FV','FW','FX','FY','FZ','GA','GB','GC','GD','GE','GF','GG','GH','GI','GJ','GK','GL','GM','GN','GO','GP','GQ','GR','GS','GT','GU','GV','GW','GX','GY','GZ','HA','HB','HC','HD','HE','HF','HG','HH','HI','HJ','HK','HL','HM','HN','HO','HP','HQ','HR','HS','HT','HU','HV','HW','HX','HY','HZ','IA','IB','IC','ID','IE','IF','IG','IH','II','IJ','IK','IL','IM','IN','IO','IP','IQ','IR','IS','IT','IU','IV','IW','IX','IY','IZ','JA','JB','JC','JD','JE','JF','JG','JH','JI','JJ','JK','JL','JM','JN','JO','JP','JQ','JR','JS','JT','JU','JV','JW','JX','JY','JZ','KA','KB','KC','KD','KE','KF','KG','KH','KI','KJ','KK','KL','KM','KN','KO','KP','KQ','KR','KS','KT','KU','KV','KW','KX','KY','KZ','LA','LB','LC','LD','LE','LF','LG','LH','LI','LJ','LK','LL','LM','LN','LO','LP','LQ','LR','LS','LT','LU','LV','LW','LX','LY','LZ','MA','MB','MC','MD','ME','MF','MG','MH','MI','MJ','MK','ML','MM','MN','MO','MP','MQ','MR','MS','MT','MU','MV','MW','MX','MY','MZ','NA','NB','NC','ND','NE','NF','NG','NH','NI','NJ','NK','NL','NM','NN','NO','NP','NQ','NR','NS','NT','NU','NV','NW','NX','NY','NZ','OA','OB','OC','OD','OE','OF','OG','OH','OI','OJ','OK','OL','OM','ON','OO','OP','OQ','OR','OS','OT','OU','OV','OW','OX','OY','OZ','PA','PB','PC','PD','PE','PF','PG','PH','PI','PJ','PK','PL','PM','PN','PO','PP','PQ','PR','PS','PT','PU','PV','PW','PX','PY','PZ','QA','QB','QC','QD','QE','QF','QG','QH','QI','QJ','QK','QL','QM','QN','QO','QP','QQ','QR','QS','QT','QU','QV','QW','QX','QY','QZ','RA','RB','RC','RD','RE','RF','RG','RH','RI','RJ','RK','RL','RM','RN','RO','RP','RQ','RR','RS','RT','RU','RV','RW','RX','RY','RZ','SA','SB','SC','SD','SE','SF','SG','SH','SI','SJ','SK','SL','SM','SN','SO','SP','SQ','SR','SS','ST','SU','SV','SW','SX','SY','SZ','TA','TB','TC','TD','TE','TF','TG','TH','TI','TJ','TK','TL','TM','TN','TO','TP','TQ','TR','TS','TT','TU','TV','TW','TX','TY','TZ','UA','UB','UC','UD','UE','UF','UG','UH','UI','UJ','UK','UL','UM','UN','UO','UP','UQ','UR','US','UT','UU','UV','UW','UX','UY','UZ','VA','VB','VC','VD','VE','VF','VG','VH','VI','VJ','VK','VL','VM','VN','VO','VP','VQ','VR','VS','VT','VU','VV','VW','VX','VY','VZ','WA','WB','WC','WD','WE','WF','WG','WH','WI','WJ','WK','WL','WM','WN','WO','WP','WQ','WR','WS','WT','WU','WV','WW','WX','WY','WZ','XA','XB','XC','XD','XE','XF','XG','XH','XI','XJ','XK','XL','XM','XN','XO','XP','XQ','XR','XS','XT','XU','XV','XW','XX','XY','XZ','YA','YB','YC','YD','YE','YF','YG','YH','YI','YJ','YK','YL','YM','YN','YO','YP','YQ','YR','YS','YT','YU','YV','YW','YX','YY','YZ','ZA','ZB','ZC','ZD','ZE','ZF','ZG','ZH','ZI','ZJ','ZK','ZL','ZM','ZN','ZO','ZP','ZQ','ZR','ZS','ZT','ZU','ZV','ZW','ZX','ZY','ZZ','AAA','AAB','AAC','AAD','AAE','AAF','AAG','AAH','AAI','AAJ','AAK','AAL','AAM','AAN','AAO','AAP','AAQ','AAR','AAS','AAT','AAU','AAV','AAW','AAX','AAY','AAZ','ABA','ABB','ABC','ABD','ABE','ABF','ABG','ABH','ABI','ABJ','ABK','ABL','ABM','ABN','ABO','ABP','ABQ','ABR','ABS','ABT','ABU','ABV','ABW','ABX','ABY','ABZ','ACA','ACB','ACC','ACD','ACE','ACF','ACG','ACH','ACI','ACJ','ACK','ACL','ACM','ACN','ACO','ACP','ACQ','ACR','ACS','ACT','ACU','ACV','ACW','ACX','ACY','ACZ','ADA','ADB','ADC','ADD','ADE','ADF','ADG','ADH','ADI','ADJ','ADK','ADL','ADM','ADN','ADO','ADP','ADQ','ADR','ADS','ADT','ADU','ADV','ADW','ADX','ADY','ADZ','AEA','AEB','AEC','AED','AEE','AEF','AEG','AEH','AEI','AEJ','AEK','AEL','AEM','AEN','AEO','AEP','AEQ','AER','AES','AET','AEU','AEV','AEW','AEX','AEY','AEZ','AFA','AFB','AFC','AFD','AFE','AFF','AFG','AFH','AFI','AFJ','AFK','AFL','AFM','AFN','AFO','AFP','AFQ','AFR','AFS','AFT','AFU','AFV','AFW','AFX','AFY','AFZ','AGA','AGB','AGC','AGD','AGE','AGF','AGG','AGH','AGI','AGJ','AGK','AGL','AGM','AGN','AGO','AGP','AGQ','AGR','AGS','AGT','AGU','AGV','AGW','AGX','AGY','AGZ','AHA','AHB','AHC','AHD','AHE','AHF','AHG','AHH','AHI','AHJ','AHK','AHL','AHM','AHN','AHO','AHP','AHQ','AHR','AHS','AHT','AHU','AHV','AHW','AHX','AHY','AHZ','AIA','AIB','AIC','AID','AIE','AIF','AIG','AIH','AII','AIJ','AIK','AIL','AIM','AIN','AIO','AIP','AIQ','AIR','AIS','AIT','AIU','AIV','AIW','AIX','AIY','AIZ','AJA','AJB','AJC','AJD','AJE','AJF','AJG','AJH','AJI','AJJ','AJK','AJL','AJM','AJN','AJO','AJP','AJQ','AJR','AJS','AJT','AJU','AJV','AJW','AJX','AJY','AJZ','AKA','AKB','AKC','AKD','AKE','AKF','AKG','AKH','AKI','AKJ','AKK','AKL','AKM','AKN','AKO','AKP','AKQ','AKR','AKS','AKT','AKU','AKV','AKW','AKX','AKY','AKZ','ALA','ALB','ALC','ALD','ALE','ALF','ALG','ALH','ALI','ALJ','ALK','ALL','ALM','ALN','ALO','ALP','ALQ','ALR','ALS','ALT','ALU','ALV','ALW','ALX','ALY','ALZ','AMA','AMB','AMC','AMD','AME','AMF','AMG','AMH','AMI','AMJ'];
	
	/**
	 * VoiceGopher is a child of AlexaSkill.
	 * To read more about inheritance in JavaScript, see the link below.
	 *
	 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript#Inheritance
	 */
	
	
	var VoiceGopher = function () {
	    AlexaSkill.call(this, APP_ID);
	};
	
	// Extend AlexaSkill
	VoiceGopher.prototype = Object.create(AlexaSkill.prototype);
	VoiceGopher.prototype.constructor = VoiceGopher;
	
	VoiceGopher.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
		var speechText = "Welcome to VoiceGopher Skill. You can ask VoiceGopher what is standard deviation of column number 1 in spreadsheet 0. Make sure to link your amazon account first by asking VoiceGopher to link my account with code provided in website. ";
		//var speechText = "Welcome to VoiceGopher Help. You can make requests like: Ask VoiceGopher what is highest value of column number 1 in spreadsheet 1";
	    // If the user either does not reply to the welcome message or says something that is not
	    // understood, they will be prompted again with this text.
	    var repromptText = "For instructions on what you can say, please say help me or visit our website www.voicegopher.com.";
	    console.log("launch");
	    response.ask(speechText, repromptText);
	};
	
	
	
	
	VoiceGopher.prototype.eventHandlers.onIntent = function (intentRequest, session, response) {
        var intent = intentRequest.intent,
            intentName = intentRequest.intent.name,
            intentHandler = this.intentHandlers[intentName];
        
        if (intentHandler) {
            console.log('dispatch intent = ' + intentName);
            var alexa_uid = session.user.userId
            console.log('Alexa userId = ' + session.user.userId);
            if(intentName =='LinkAccountIntents' || 
            		intentName =='AMAZON.StopIntent' || 
            		intentName =='AMAZON.CancelIntent' ||
            		intentName =='AMAZON.HelpIntent') {
            	intentHandler.call(this, intent, session, response);
            } else {
	            security.getUser(alexa_uid, function(err, user) {
	            	console.log("User " + JSON.stringify(user));
	            	console.log("intent.slots " + JSON.stringify(intent.slots));
	            	var fileNumber = intent.slots.FileNumber.value;
	            	//var extension = intent.slots.Extension.value;
	            	fileName = "spreadsheet_" +fileNumber ;
	            	params.Key = fileName;
	            	console.log("fileName " + fileName);
	            	if(user && user.confirmed) {
	            		console.log('Alexa userId bucket = ' + user.bucket);
	            		//session.user.bucket = user.bucket;
	            		if(user.bucket)
	            			params.Bucket = user.bucket;
	            		console.log('params.Bucket = ' + params.Bucket);
	            		console.log('params.Key = ' + params.Key);
	            		intentHandler.call(this, intent, session, response);
	            	} else {
	            		
	            		var speechOutput = {
	    				    speech: accountNotLinkedtext,
	    				    type: AlexaSkill.speechOutputType.PLAIN_TEXT
	    				};
	            		var cardHeading ="Account not linked";
	            		var cardText = accountNotLinkedtext;
	    				response.tellWithCard(speechOutput, cardHeading, cardText);
	            	}
	            });
            }
        } else {
            throw 'Unsupported intent = ' + intentName;
        }
    },
	
    VoiceGopher.prototype.eventHandlers.onSessionStarted =  function (sessionStartedRequest, session) {
		console.log("onSessionStarted");
	};
	
	var s3 = new AWS.S3();
	var global_data = [];
	var global_headers = [];
	
	var fileName="demo.csv";
	var defaultBucket = "voice-query";
	var params = {
        Bucket: defaultBucket,
        Key: fileName
	};
    var array, result;
    
    function processData(csv, contentType) {
        var allTextLines = csv.split(/\r?\n/);
        var lines = [];
        var delim = delimDetector.getDelimByContentType(contentType);
        global_headers = allTextLines[0].split(delim);
        
        console.log("allTextLines.length " + allTextLines.length);
        for (var i=1; i<allTextLines.length; i++) {
            var data = allTextLines[i].split(delim);
                var tarr = [];
                for (var j=0; j<data.length; j++) {
                    tarr.push(data[j]);
                }
                //console.log("lines" + lines);
                lines.push(tarr);
                
                global_data = lines;
             
        }
        console.log(lines.toString('utf8'));
    }
	
	Array.prototype.findIndexTitle = function (value) {
	    for (var i = 0; i < this.length; i++) {
	      if (this[i].toLowerCase().search(value.toLowerCase()) >= 0) { return i;}
	    }
	    return -1;
	}
	
	function getIndexByExcelColumnName(excelColumnName) {
		var base = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', i, j, result = 0;

		  for (i = 0, j = excelColumnName.length - 1; i < excelColumnName.length; i += 1, j -= 1) {
		    result += Math.pow(base.length, j) * (base.indexOf(excelColumnName[i]) + 1);
		  }

		  return result-1;
	}
	
	var getMaxOfColumn = function(index, callback) {
		s3.getObject(params, function(err, data) {
	        if (!err && data!=null) { 
	        	var attachment, file;
	         	file = new Buffer(data.Body, 'binary');
	            processData (file.toString('utf8').replace(/\0/g, ''), data.ContentType);
	            var max = maxOfColumn(index);
		      	callback( max);
	        } else {
	        	console.log("err " + err);
	    		console.log("data " + data);
	        	var errResponse =  {'max' : 'Unknown'};
	        	callback(errResponse);
	        }
	    });
	}	

	var getMaxOfColumnByName = function(columnName, callback) {
	    s3.getObject(params, function(err, data) {
	    	if (!err && data!=null) { 
	        	var attachment, file;
	         	file = new Buffer(data.Body, 'binary');
	            processData (file.toString('utf8').replace(/\0/g, ''), data.ContentType);
	            var index = getIndexByExcelColumnName(columnName);
		        var max = maxOfColumn(index);
		      	callback( max);
	    	} else {
	    		console.log("err " + err);
	    		console.log("data " + data);
	        	var errResponse =  {'max' : 'Unknown'};
	        	callback(errResponse);
	        }
	    });
	}
	
	function maxOfColumn(index) {
		  var numArray= [];
		  var max = parseFloat(global_data[0][index]);
		  var ind = [];
		  
		  console.log('global_data[0]['+index+']',global_data[0][index]);
		  for (var i=0; i<global_data.length; ++i) {
		    
		    if (parseFloat(global_data[i][index]) >= max)
		    {   console.log('new max', global_data[i][index], i);
		        if ((i >0)&&(max == global_data[i][index] )){ind.push(i)}
		        else{ind = []; ind.push(i);}
		            
		        max = global_data[i][index];
		        
		    } 
		  }
		  
		  return {'max' : max, 'i': ind};
		}
	
	
	var getMinOfColumn = function(index, callback) {
	    s3.getObject(params, function(err, data) {
	    	if (!err && data!=null) { 
	        	console.log("data " + JSON.stringify(data));
	    		var attachment, file;
	         	file = new Buffer(data.Body, 'binary');
	         	//var csv = file.toString('utf8').split(/\r?\n/);
	            processData (file.toString('utf8').replace(/\0/g, ''), data.ContentType);
	            var min = minOfColumn(index);
	            callback(min);
	    	} else {
	    		console.log("err " + err);
	    		console.log("data " + data);
	        	var errResponse =  {'min' : 'Unknown'};
	        	callback(errResponse);
	        }
	    });
	}
	
	var getMinOfColumnByName = function(columnName, callback) {
	    s3.getObject(params, function(err, data) {
	    	if (!err && data!=null) { 
	        	var attachment, file;
	         	file = new Buffer(data.Body, 'binary');
	         	//var csv = file.toString('utf8').split(/\r?\n/);
	            processData (file.toString('utf8').replace(/\0/g, ''), data.ContentType);
	            var index = getIndexByExcelColumnName(columnName);
	            var min = minOfColumn(index);
	            callback(min);
	    	} else {
	    		console.log("err " + err);
	    		console.log("data " + data);
	        	var errResponse =  {'min' : 'Unknown'};
	        	callback(errResponse);
	        }
	    });
	}
	
	function minOfColumn(index) {
		  var numArray, ind = [];
		  var min = parseFloat(global_data[1][index]);
		  
		  for (var i=0; i<global_data.length-1; i++) {
		    if (parseFloat(global_data[i][index]) <= min)
		      {   if ((i >0)&&(min == global_data[i][index] )){ind.push(i)}
		          else{ind = []; ind.push(i);}
		          min = global_data[i][index];} 
		    
		  }
		  
		  return {'min' : min, 'i': ind};
	}
	
	
	function sortByHash(hash)
	{
	  var sort_array = [];
	  for (var k in hash) {
	    if (hash.hasOwnProperty(k)) {
	      sort_array[hash[k]] = k;
	    }
	  }
	  var k=0;
	  
	  global_data.sort(function(a,b){
	    
	    for (var i=0; i< sort_array.length; i++) {
	      indexColumn = global_headers.findIndexTitle(sort_array[i]);
	      if (a[indexColumn] === undefined || a[indexColumn]=== null){a[indexColumn] = 0;}
	      if (b[indexColumn] === undefined || b[indexColumn]=== null){b[indexColumn] = 0;}
	      
	      if (a[indexColumn] < b[indexColumn]) { return -1;}
	      if (a[indexColumn] > b[indexColumn]) { return 1;};
	    }
	    
	   
	    return 0;
	    
	  });
	  
	}
	
	var getMeanOfColumnByName = function(columnName, callback) {
	    s3.getObject(params, function(err, data) {
	    	if (!err && data!=null) { 
	        	var attachment, file;
         		file = new Buffer(data.Body, 'binary');
         		processData (file.toString('utf8').replace(/\0/g, ''), data.ContentType);
         		var index = getIndexByExcelColumnName(columnName);
         		var mean = meanOfColumn(index);
         		callback( mean);
	    	} else {
	    		console.log("err " + err);
	    		console.log("data " + data);
	        	var errResponse =  'Unknown';
	        	callback(errResponse);
	        }	
	    });
	}
	
	var getMeanOfColumn = function(index, callback) {
	    s3.getObject(params, function(err, data) {
	    	if (!err && data!=null) { 
	        	var attachment, file;
         		file = new Buffer(data.Body, 'binary');
         		processData (file.toString('utf8').replace(/\0/g, ''), data.ContentType);
         		var mean = meanOfColumn(index);
         		callback( mean);
	    	} else {
	    		console.log("err " + err);
	    		console.log("data " + data);
	        	var errResponse =  'Unknown';
	        	callback(errResponse);
	        }	
	    });
	}
	
	function meanOfColumn(index) {
        var numArray= [];
        var min = parseFloat(global_data[1][index]);
        var sum = 0.0;
        for (var i=0; i< global_data.length-1; i++) {
            sum= sum + parseFloat(global_data[i][index]);
        }
        var mean = sum/parseFloat(global_data.length-1);
        return mean;
	}
	
	var getSumOfColumnByName = function(columnName, callback) {
	    s3.getObject(params, function(err, data) {
	    	if (!err && data!=null) { 
	        	var attachment, file;
         		file = new Buffer(data.Body, 'binary');
         		processData (file.toString('utf8').replace(/\0/g, ''), data.ContentType);
         		var index = getIndexByExcelColumnName(columnName);
         		var sum = sumOfColumn(index);
         		callback( sum);
	    	} else {
	    		console.log("err " + err);
	    		console.log("data " + data);
	        	var errResponse =  'Unknown';
	        	callback(errResponse);
	        }	
	    });
	}
	
	var getSumOfColumn = function(index, callback) {
	    s3.getObject(params, function(err, data) {
	    	if (!err && data!=null) { 
	        	var attachment, file;
         		file = new Buffer(data.Body, 'binary');
         		processData (file.toString('utf8').replace(/\0/g, ''), data.ContentType);
         		var sum = sumOfColumn(index);
         		callback( sum);
	    	} else {
	    		console.log("err " + err);
	    		console.log("data " + data);
	        	var errResponse =  'Unknown';
	        	callback(errResponse);
	        }	
	    });
	}
	
	
	function sumOfColumn(index) {
	  var numArray= [];
	  var min = parseFloat(global_data[1][index]);
	  var sum = 0.0;
	  for (var i=0; i< global_data.length-1; i++) {
	    sum= sum + parseFloat(global_data[i][index]);
	  }
	  return sum;
	}
	
	var getSTDeviationOfColumnByName = function(columnName, callback) {
	    s3.getObject(params, function(err, data) {
	    	if (!err && data!=null) { 
	        	var attachment, file;
         		file = new Buffer(data.Body, 'binary');
         		processData (file.toString('utf8').replace(/\0/g, ''), data.ContentType);
         		var index = getIndexByExcelColumnName(columnName);
         		var stDeviation = stDeviationOfColumn(index);
         		callback(stDeviation);
	    	} else {
	    		console.log("err " + err);
	    		console.log("data " + data);
	        	var errResponse =  'Unknown';
	        	callback(errResponse);
	        }	
	    });
	}
	
	var getSTDeviationOfColumn = function(index, callback) {
	    s3.getObject(params, function(err, data) {
	    	if (!err && data!=null) { 
	        	var attachment, file;
         		file = new Buffer(data.Body, 'binary');
         		processData (file.toString('utf8').replace(/\0/g, ''), data.ContentType);
         		console.log("calling stDeviation");
         		var stDeviation = stDeviationOfColumn(index);
         		callback(stDeviation);
	    	} else {
	    		console.log("err " + err);
	    		console.log("data " + data);
	        	var errResponse =  'Unknown';
	        	callback(errResponse);
	        }	
	    });
	}
	
	function stDeviationOfColumn(index) {
	  var numArray= [];
	  var mean = meanOfColumn(index)
	  var sum = 0.0;
	  for (var i=0; i< global_data.length-1; i++) {
	      sum= sum + Math.pow((parseFloat(global_data[i][index])-mean),2);
	  }
	  mean = sum/parseFloat(global_data.length-1);
	  return Math.sqrt(mean);
	}
	
	
	var getRangeByName = function(min, max, columnName, callback) {
	    s3.getObject(params, function(err, data) {
	        if (!err) 
	        	var attachment, file;
         		file = new Buffer(data.Body, 'binary');
         		processData (file.toString('utf8').replace(/\0/g, ''), data.ContentType);
         		var index = getIndexByExcelColumnName(columnName);
         		var rangeRes = range(min, max, index);
         		callback(rangeRes);
	    });
	}
	
	var getRange = function(min, max, index, callback) {
	    s3.getObject(params, function(err, data) {
	        if (!err) 
	        	var attachment, file;
         		file = new Buffer(data.Body, 'binary');
         		processData (file.toString('utf8').replace(/\0/g, ''), data.ContentType);
         		var rangeRes = range(min, max, index);
         		callback(rangeRes);
	    });
	}
	
	function range(min, max, indexOfColumn)
	{
	   var res = [];
	   for (var i = 0; i < global_data.length; i++) {
	     if ((min <= global_data[i][indexOfColumn])&&( global_data[i][indexOfColumn] <= max ))
	     {res.push(global_data[i][indexOfColumn]);};
	   }
	   return res;  
	}

	VoiceGopher.prototype.intentHandlers = {
	 
	 "LinkAccountIntents": function (intent, session, response) {
        var confirmationCode = intent.slots.ConfirmationCode.value;
        console.log('ConfirmationCode=', confirmationCode);
        var positiveSpeech = "The account was successfully linked with code " + confirmationCode;
        var negativeSpeech = "Provided activation code did not match. please check again";
        console.log("userId " + session.user.userId);
        if(confirmationCode) {
	        security.linkAccount(confirmationCode, session.user.userId, function(err, data) {
	        	console.log("data " + data);
		        var speech = positiveSpeech;
		        var cardHeading ="Account linked";
	        	if (data=='NO_USER_FOUND') {
		        	speech = negativeSpeech;
		        	cardHeading = "Account not linked";
		        } 
	        	var speechOutput = {
		            speech: speech,
		            type: AlexaSkill.speechOutputType.PLAIN_TEXT
		        };
	        	
	    		var cardText = speech;
		        response.tellWithCard(speechOutput, cardHeading, cardText);
	        });
		} else {
			var undefinedConfirmationCodeSpeech = "There was a problem with recognizing the confirmation code. Please try again.";
			var speechOutput = {
	            speech: undefinedConfirmationCodeSpeech,
	            type: AlexaSkill.speechOutputType.PLAIN_TEXT
		    };
			var cardHeading = "Account not linked";	
    		var cardText = undefinedConfirmationCodeSpeech;
	        response.tellWithCard(speechOutput, cardHeading, cardText);
		}
	 },		
	 
	 "MinNumberIntents": function (intent, session, response) {
            var indexColumn = intent.slots.ColumnIndex.value;
            getMinOfColumn(indexColumn, function(min){
            	console.log('min=', min);
                var speech = "The lowest element is " + min.min;
                console.log("speech " + speech);
                var speechOutput = {
                    speech: speech,
                    type: AlexaSkill.speechOutputType.PLAIN_TEXT
                };
                var cardText = speech;
                response.tellWithCard(speechOutput, minCardHeading, cardText);
            });
        },
	    "MinNumberCustomIntents": function (intent, session, response) {
	        var columnName = intent.slots.ColumnName;
	        console.log('columnName=', columnName.value);
	        getMinOfColumnByName(columnName.value, function(min){
                console.log('min=', min);
    	        var speech = "The lowest element is " + min.min;
    			console.log("speech " + speech);  
    			var speechOutput = {
    			    speech: speech,
    			    type: AlexaSkill.speechOutputType.PLAIN_TEXT
    			};
    			var cardText = speech;
                response.tellWithCard(speechOutput, minCardHeading, cardText);
        
            });
	    },
	    "MaxNumberIntents": function (intent, session, response) {
	    	var indexColumn = intent.slots.ColumnIndex.value;
            getMaxOfColumn(indexColumn, function(max){
                console.log('max=', max);
    	        var speech = "The highest element is " + max.max;
    			console.log("speech " + speech);  
    			var speechOutput = {
    			    speech: speech,
    			    type: AlexaSkill.speechOutputType.PLAIN_TEXT
    			};
    			var cardText = speech;
                response.tellWithCard(speechOutput, maxCardHeading, cardText);
            });
	    
	    },
	    "MaxNumberCustomIntents": function (intent, session, response) {
	    	var columnName = intent.slots.ColumnName;
	        console.log('columnName=', columnName.value);
	        getMaxOfColumnByName(columnName.value, function(max){
                console.log('max=', max);
    	        var speech = "The highest element is " + max.max;
    			console.log("speech " + speech);  
    			var speechOutput = {
    			    speech: speech,
    			    type: AlexaSkill.speechOutputType.PLAIN_TEXT
    			};
    			var cardText = speech;
                response.tellWithCard(speechOutput, maxCardHeading, cardText);
        
            });
	    
	    },
	    "AvgNumberIntents": function (intent, session, response) {
	    	var indexColumn = intent.slots.ColumnIndex.value;
            getMeanOfColumn(indexColumn, function(avg){
                console.log('avg=', avg);
    	        var speech = "The average value is " + avg;
    			console.log("speech " + speech);  
    			var speechOutput = {
    			    speech: speech,
    			    type: AlexaSkill.speechOutputType.PLAIN_TEXT
    			};
    			var cardText = speech;
                response.tellWithCard(speechOutput, avgCardHeading, cardText);
            });
	    },
	    "AvgNumberCustomIntents": function (intent, session, response) {
	    	var columnName = intent.slots.ColumnName;
            getMeanOfColumnByName(columnName.value, function(avg){
                console.log('avg=', avg);
    	        var speech = "The average value is " + avg;
    			console.log("speech " + speech);  
    			var speechOutput = {
    			    speech: speech,
    			    type: AlexaSkill.speechOutputType.PLAIN_TEXT
    			};
    			var cardText = speech;
                response.tellWithCard(speechOutput, avgCardHeading, cardText);
            });
	    },
	    "StDevNumberIntents": function (intent, session, response) {
	    	var indexColumn = intent.slots.ColumnIndex.value;
	    	getSTDeviationOfColumn(indexColumn, function(stDev){
    	        var speech = "The standard deviation is " + stDev;
    			console.log("speech " + speech);  
    			var speechOutput = {
    			    speech: speech,
    			    type: AlexaSkill.speechOutputType.PLAIN_TEXT
    			};
    			var cardText = speech;
                response.tellWithCard(speechOutput, stDevCardHeading, cardText);
            });
	    },
	    "StDevNumberCustomIntents": function (intent, session, response) {
	    	var columnName = intent.slots.ColumnName;
	    	getSTDeviationOfColumnByName(columnName.value, function(stDev){
                console.log('stDev=', stDev);
    	        var speech = "The standard deviation is " + stDev;
    			console.log("speech " + speech);  
    			var speechOutput = {
    			    speech: speech,
    			    type: AlexaSkill.speechOutputType.PLAIN_TEXT
    			};
    			var cardText = speech;
                response.tellWithCard(speechOutput, stDevCardHeading, cardText);
            });
	    },
	    "SumNumberIntents": function (intent, session, response) {
	    	var indexColumn = intent.slots.ColumnIndex.value;
            getSumOfColumn(indexColumn, function(sum){
                console.log('sum = ', sum);
    	        var speech = "The total is " + sum;
    			console.log("speech " + speech);  
    			var speechOutput = {
    			    speech: speech,
    			    type: AlexaSkill.speechOutputType.PLAIN_TEXT
    			};
    			var cardText = speech;
                response.tellWithCard(speechOutput, sumCardHeading, cardText);
            });
	    },
	    "SumNumberCustomIntents": function (intent, session, response) {
	    	var columnName = intent.slots.ColumnName;
            getSumOfColumnByName(columnName.value, function(sum){
                console.log('sum = ', sum);
    	        var speech = "The total is " + sum;
    			console.log("speech " + speech);  
    			var speechOutput = {
    			    speech: speech,
    			    type: AlexaSkill.speechOutputType.PLAIN_TEXT
    			};
    			var cardText = speech;
                response.tellWithCard(speechOutput, sumCardHeading, cardText);
            });
	    },
	    "MinDateIntents": function (intent, session, response) {
	        response.tellWithCard("", "cardTitle", "empty");
	    
	    },
	    "MinDateCustomIntents": function (intent, session, response) {
	        response.tellWithCard("", "cardTitle", "empty");
	    
	    },
	    "MaxDateIntents": function (intent, session, response) {
	        response.tellWithCard("", "cardTitle", "empty");
	    
	    },
	    "MaxDateCustomIntents": function (intent, session, response) {
	        response.tellWithCard("", "cardTitle", "empty");
	    
	    },
	    "AggregateTextIntents": function (intent, session, response) {
	        response.tellWithCard("", "cardTitle", "empty");
	    
	    },
	    "AggregateTextCustomIntents": function (intent, session, response) {
	        response.tellWithCard("", "cardTitle", "empty");
	    
	    },
	    "AMAZON.StopIntent": function (intent, session, response) {
	    	response.tellWithCard("", "StopIntent", "StopIntent");
	    },
	
	    "AMAZON.CancelIntent": function (intent, session, response) {
	    	response.tellWithCard("", "CancelIntent", "CancelIntent");
	    },
	
	    "AMAZON.HelpIntent": function (intent, session, response) {
	    	var speechText;
	    	var repromptText;
	    	var alexa_uid = session.user.userId; 
	    	security.getUser(alexa_uid, function(err, user) {
	            	if(user && user.confirmed) {
	            		speechText = "You can ask VoiceGopher what is standard deviation of column number 1 in spreadsheet 0";
	        	    	repromptText = "You can also ask VoiceGopher what is total of column number 1 in spreadsheet 0";
	            	} else {
	            		speechText = "First you need to go to our website www.voicegopher.com and authenticate via Amazon. You will be provided code. Secondly You would need to link by saying Alexa tell VoiceGopher to link my account with code that website provided. Now you can upload spreadsheets and invoke statistical methods.";
	        	    	repromptText = "For example you can ask VoiceGopher what is standard deviation of column number 1 in spreadsheet 0";
	            	}
	            
			        var speechOutput = {
			            speech: speechText,
			            type: AlexaSkill.speechOutputType.PLAIN_TEXT
			        };
			        var repromptOutput = {
			            speech: repromptText,
			            type: AlexaSkill.speechOutputType.PLAIN_TEXT
			        };
			        response.ask(speechOutput, repromptOutput);
	    	});
	    }
	};
	
	
	
	exports.handler = function (event, context) {
	    console.log(JSON.stringify(event));
	    console.log(JSON.stringify(context));
		
	    var voiceGopher = new VoiceGopher();
	    voiceGopher.execute(event, context);
	    
	    
	};
	
	
